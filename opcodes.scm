(define-syntax-rule (add . props)
  (+ . props))

(define-syntax-rule (sub . props)
  (- . props))

(define-syntax-rule (mul . props)
  (* . props))


(define-syntax-rule (div . props)
  (/ . props))

(define-syntax-rule (mod . props)
  (modulo . props))

(define-syntax-rule (equ? x value value* ...)
  (memq #t
        (map (lambda (y)
               (equal? x y))
             '(value value* ...))))
