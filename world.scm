(define game-verbs '(quit, save, script, restore, unscript, verbose, $verify))
(define here #f)
(define it #f)
(define objects '())
(define prsa #f)
(define prsi #f)
(define prso #f)
(define rooms '())

(define-syntax-rule (object name value value* ...)
  (begin
    (define name '(value value* ...))
    (set! objects
          (cons 'name objects))))

(define-syntax-rule (room name value value* ...)
  (begin
    (define name '(value value* ...))
    (set! rooms
          (cons 'name rooms))))

(define-syntax-rule (getp obj pr)
  (cdr (assoc 'pr obj)))

(define-syntax-rule (putp obj pr val)
  (set! obj
        (assoc-set! obj
                    'pr
                    (cons 'val '()))))

(define-syntax-rule (loc obj)
  (car (getp obj loc)))

(define-syntax-rule (in? containee container)
  (equal? (loc containee)
          'container))

(define-syntax-rule (fset? obj flag)
  (if (memq 'flag (getp obj flags))
      #t
      #f))

(define-syntax-rule (fset obj flag)
  (set! obj
        (assoc-set! obj
                    'flags
                    (cons 'flag
                          (getp obj flags)))))

(define-syntax-rule (fclear obj flag)
  (set! obj
        (assoc-set! obj
                    'flags
                    (filter (lambda (x)
                              (not (equal? x 'flag)))
                            (getp obj flags)))))

(define-syntax-rule (move obj container)
  (putp obj loc container))

(define-syntax-rule (remove obj)
  (move obj #f))
